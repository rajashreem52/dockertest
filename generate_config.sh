#!/bin/bash

imagename="my-docker-image"

i=1

case "$OSTYPE" in
  solaris*) echo "SOLARIS" ;;
  darwin*)  echo "OSX" ;; 
  linux*)   echo "LINUX" ;;
  bsd*)     echo "BSD" ;;
  msys*)    echo "WINDOWS" ;;
  cygwin*)  echo "ALSO WINDOWS" ;;
  *)        echo "unknown: $OSTYPE" ;;
esac


array=( $(docker run -v ~/newlogs2:/linking  --name=new18 my-docker-image /bin/bash dockerlisttest.sh linking| tr -d '\r') )

arr=()

idx=0


for element in "${array[@]}"

do

  length=${#element}
  testname=${element:2:$((length-1))}
  arr[idx]=$testname
  idx=$((idx + 1))
done

for testname in "${arr[@]}"
do
   echo "job_${testname}:
   tags:
   - doc
   image: my-docker-image 
   script:
    - ./runtests ${testname}
    ">>child_pipeline.yml

  i=$((i + 1))
  
done
